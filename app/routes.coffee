Route = App.router

###
vault = require('node-vault')()

vault.init(secret_shares: 1, secret_threshold: 1)
  .then (result) ->
    keys = result.keys
    vault.token = result.root_token # set token for all following requests
    vault.unseal secret_shares: 1, secret_threshold: keys[0] # unseal vault server
    console.log vault.token
  .catch (error) ->
    console.error error

vault.write('secret/hello', value: 'some_secret')
  .then ->
    value = vault.read 'secret/hello'
    console.log value
  .catch (error) ->
    console.error error
###

Route.get '/', (request, response) ->
  response.view 'index', title: 'Quorra Coffee'

Route.get 'users', (request, response) ->
  response.send 'Users.'
